"""
Analizador léxico simple para contabilizar el número de incidencias
de una palabra en un texto.
"""


class AnalizadorLexico:

    def __init__(self, txt):
        self.texto = txt

    def incidencias(self, palabra):
        incidencias = 0

        token = ""                          # Guarda el token encontrado
        fin_token = False                   # Indica si se encontro un token
        for caracter in self.texto:
            if caracter.isalnum():          # Vefica si el caractes no es un numero o palabra
                token += caracter
                fin_token = True
            else:
                fin_token = False

            if not fin_token and token:     # Si se encontro un token se compara con la palabra
                if token == palabra:
                    incidencias += 1
                token = ""

        if fin_token and token:             # Verifica el ultimo token
            if token == palabra:
                incidencias += 1

        return incidencias


# Unit tests
if __name__ == '__main__':
    texto = """
        La logística Digital es un concepto que surge de la integración entre la logística tradicional y
        la era digital. Con el auge del correo electrónico y las descargas digitales reemplazando
        productos físicos, podríamos estar hablando de un golpe devastador para la industria de la
        logística, pero, de hecho, ha ocurrido algo muy diferente. El sector de la logística ha
        introducido las innovaciones digitales.
    """
    analizador = AnalizadorLexico(texto)
    incidencia = analizador.incidencias('logística')
    print(f'{incidencia} incidencia encontradas')
