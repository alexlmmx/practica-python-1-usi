import sys
import os
from utils.analizador import AnalizadorLexico


if __name__ == '__main__':
    nombre_texto, palabra = sys.argv[1], sys.argv[2]         # Se obtiene el nombre del texto y la palabra a buscar

    if nombre_texto:
        with open(os.path.join(sys.path[0], nombre_texto)) as texto:
            str_texto = texto.read()
            analizador = AnalizadorLexico(str_texto)
            incidencias = analizador.incidencias(palabra)
            print(f'{incidencias} incidencias encontradas')