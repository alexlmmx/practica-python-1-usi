# USI Practica 1

### Explicacion
Este script es un programa simple que se encarga de contar el número de veces que aparece una determinada palabra en un texto.

Para ello, primero se importan las librerías necesarias: `sys`, `os`, y la clase AnalizadorLexico del módulo `utils.analizador`.

Luego, se obtienen el nombre del archivo de texto y la palabra a buscar a partir de los argumentos pasados al script al ejecutarlo.

Posteriormente, se abre el archivo de texto y se lee su contenido completo. A continuación, se crea una instancia de AnalizadorLexico pasándole el texto leído como argumento, y se llama al método incidencias de esta instancia, pasándole la palabra a buscar como argumento. Finalmente, se imprime por pantalla el número de incidencias encontradas.

En resumen, este código permite contar el número de veces que aparece una palabra en un archivo de texto dado.

### Ejecución

Ejemplo 1:
`python3 main.py texto_1.txt logística`

Ejemplo 2:
`python3 main.py texto_2.txt manzanas`
